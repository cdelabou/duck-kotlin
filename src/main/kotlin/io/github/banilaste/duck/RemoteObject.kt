package io.github.banilaste.duck

import io.github.banilaste.duck.marshall.unmarshall
import java.nio.ByteBuffer
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.superclasses
import kotlin.reflect.typeOf

/**
 * Remote objects base class, provide also many static methods to handle remote objects
 */
abstract class RemoteObject(val identifier: RemoteObjectIdentifier) {
    init {
        // Register object if not stub
        if (!isStub(this)) {
            // Save into local remote object list
            registered[identifier.code] = this
        }
    }

    override fun equals(other: Any?): Boolean {
        return if (other is RemoteObject) identifier == other.identifier else false
    }

    companion object {
        private var idIndex = 0

        val registered = HashMap<Int, RemoteObject>()
        val factories = HashMap<KClass<out RemoteObject>, (id: RemoteObjectIdentifier) -> RemoteObject>()

        /**
         * Generate a new local identifier for a remote object hosted here
         */
        fun localIdentifier(): RemoteObjectIdentifier {
            while (registered[idIndex] != null) idIndex++
            return RemoteObjectIdentifier(
                idIndex,
                DuckProperties.localAddress,
                DuckProperties.localPort
            )
        }

        /**
         * Return the nearest @RemoteInterface among superclasses, the @RemoteInterface parent should
         * contains annotations for @RemoteMember giving information about members id
         */
        fun interfaceType(clazz: KClass<out RemoteObject>): KClass<out RemoteObject> {
            var current: KClass<*> = clazz

            while (current.findAnnotation<RemoteInterface>() == null && current != RemoteObject::class) {
                current = current.superclasses[0]
            }

            return current as KClass<out RemoteObject>
        }

        /**
         * Returns whether or not the given object is a stub
         */
        fun isStub(obj: RemoteObject): Boolean {
            var current: KClass<*> = obj::class

            while (current.findAnnotation<RemoteStub>() == null && current != RemoteObject::class) {
                current = current.superclasses[0]
            }

            return current.findAnnotation<RemoteStub>() != null
        }

        /**
         * Create a object of the same super remote interface type, which should be a stub of a
         * remote object (ie. calling a method on the object shall call a method on the remote
         * object)
         */
        fun <T: RemoteObject> stub(id: RemoteObjectIdentifier, clazz: KClass<T>): T {
            var current: KClass<*> = clazz

            while (factories[current] == null && current != RemoteObject::class) {
                current = current.superclasses[0]
            }

            if (factories[current] == null) {
                throw Error("unable to init instance of ${clazz.simpleName}, no provider")
            }

            return factories[current]!!.invoke(id) as T
        }

        /**
         * Retrieve a local remote object from buffer
         */
        fun fromBuffer(buffer: ByteBuffer): RemoteObject {
            // Call parameters
            val identifier = unmarshall<RemoteObjectIdentifier>(buffer)
           return registered[identifier.code]!!
        }
    }
}