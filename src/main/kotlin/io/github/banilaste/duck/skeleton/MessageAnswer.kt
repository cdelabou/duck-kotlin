package io.github.banilaste.duck.skeleton

/**
 * Wrapper for the answer of a request, contains a boolean to indicate whether this result
 * should be cached to be served again in case the transmission fails
 */
data class MessageAnswer(val data: ByteArray, val save: Boolean = false)