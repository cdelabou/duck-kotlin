package io.github.banilaste.duck.skeleton

import io.github.banilaste.duck.marshall.marshall
import java.lang.reflect.InvocationTargetException
import java.nio.ByteBuffer

/**
 * Object handling the incoming messages from UDP packets.
 *
 * It calls the appropriate method, getter or setter of the local remote objects.
 * In case the message received is a answer or an error, it should return the the proxy thread
 * using the promise saved (@see Promise).
 *
 * All these behaviors are defined in the different message types (@see MessageType).
 *
 */
object Skeleton {
    private val requestResults = HashMap<String, ByteArray?>()

    /**
     * Handle an incoming message
     */
    fun handle(callId: Int, localCallId: String, buffer: ByteBuffer): ByteArray? {
        // Many message types
        val messageType = buffer.get()

        // Repeat response if it was stored earlier (in SET or CALL behavior)
        if (requestResults.containsKey(localCallId)) {
            val answer = requestResults[localCallId]!!
            return ByteBuffer.allocate(answer.size + 1)
                .put(MessageType.ANSWER.code).put(answer).array()
        }

        return try {
            // Handle the message depending on its type
            MessageType.values().find { it.code == messageType }?.handle(buffer, callId, localCallId)?.let {
                // Save result if necessary
                if (it.save) {
                    requestResults[localCallId] = it.data
                }

                // Append message type and return answer message content
                ByteBuffer.allocate(it.data.size + 1)
                    .put(MessageType.ANSWER.code)
                    .put(it.data)
                    .array()
            }
        } catch (e: InvocationTargetException) {
            // Exception : send exception message
            val message = marshall(e.targetException.localizedMessage)
            val exceptionType = marshall(e.targetException.javaClass.simpleName)

            // Returns error message content
            ByteBuffer.allocate(message.size + exceptionType.size + 1)
                .put(MessageType.ERROR.code)
                .put(exceptionType)
                .put(message)
                .array()
        }
    }
}