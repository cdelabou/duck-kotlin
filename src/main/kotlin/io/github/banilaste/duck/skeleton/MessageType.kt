package io.github.banilaste.duck.skeleton

import io.github.banilaste.duck.NoCaching
import io.github.banilaste.duck.RemoteMember
import io.github.banilaste.duck.RemoteObject
import io.github.banilaste.duck.marshall.marshall
import io.github.banilaste.duck.marshall.unmarshall
import io.github.banilaste.duck.proxy.Proxy
import io.github.banilaste.duck.util.LOGGER
import java.nio.ByteBuffer
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KParameter
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.valueParameters

/**
 * Handlers for incoming messages
 */
enum class MessageType(val code: Byte): MessageHandler {
    GET(0) {
        override fun handle(buffer: ByteBuffer, callId: Int, localCallId: String): MessageAnswer? {
            val remoteObject = RemoteObject.fromBuffer(buffer)
            val methodId = buffer.int

            val member = RemoteObject.interfaceType(remoteObject::class)
                .declaredMemberProperties.find {
                it.findAnnotation<RemoteMember>()?.memberId == methodId
            }

            LOGGER.verbose(
                "calling getter of ${remoteObject.javaClass.simpleName}@${remoteObject.identifier.code} for ${member?.name}"
            )
            return member?.getter?.call(remoteObject)?.let {
                MessageAnswer(marshall(it))
            }
        }
    },
    SET(1) {
        override fun handle(buffer: ByteBuffer, callId: Int, localCallId: String): MessageAnswer {
            // Call parameters
            val remoteObject = RemoteObject.fromBuffer(buffer)
            val methodId = buffer.int

            val member = RemoteObject.interfaceType(remoteObject::class)
                .declaredMemberProperties.find {
                it.findAnnotation<RemoteMember>()?.memberId == methodId
            }

            if (member is KMutableProperty<*>) {
                LOGGER.verbose(
                    "calling setter of ${remoteObject.javaClass.simpleName}@${remoteObject.identifier.code}" +
                            " for ${member.name}"
                )
                return member.setter.call(remoteObject, unmarshall(
                    buffer,
                    member.setter.valueParameters[0].type
                )).let {
                    // Return answer and mark for saving
                    MessageAnswer(marshall(true), true)
                }
            }

            return MessageAnswer(marshall(false), true)
        }
    },
    CALL(2) {
        override fun handle(buffer: ByteBuffer, callId: Int, localCallId: String): MessageAnswer? {
            // Call parameters
            val remoteObject = RemoteObject.fromBuffer(buffer)
            val methodId = buffer.int

            // Retrieve method
            val method = RemoteObject.interfaceType(remoteObject::class)
                .declaredFunctions.find {
                it.findAnnotation<RemoteMember>()?.memberId == methodId
            }

            return method?.let {
                // Find method params
                val callParams: Map<KParameter, Any> = method.parameters.map { it to
                        if (it.kind == KParameter.Kind.INSTANCE)
                            remoteObject
                        else
                            unmarshall(buffer, it.type) as Any
                }.toMap()


                LOGGER.verbose(
                    "calling ${method.name} of ${remoteObject.javaClass.simpleName}@${remoteObject.identifier.code}, " +
                            "arguments = ${callParams.filter { it.key.kind != KParameter.Kind.INSTANCE }.mapKeys { it.key.name }}"
                )

                // Call the method by params names
                method.callBy(callParams)?.let {
                    // Returns the marshalled result along with the saving metadata
                    MessageAnswer(
                        // Marshalled call result
                        marshall(it),
                        // Save by default unless the method is marked with @NoCache
                        method.findAnnotation<NoCaching>() == null
                    )
                }
            }

        }
    },
    ANSWER(3) {
        override fun handle(buffer: ByteBuffer, callId: Int, localCallId: String): MessageAnswer? {
            Proxy.unlock(callId, buffer)
            return null
        }
    },
    ERROR(4) {
        override fun handle(buffer: ByteBuffer, callId: Int, localCallId: String): MessageAnswer? {
            Proxy.unlock(callId, buffer, true)
            return null
        }
    };


}