package io.github.banilaste.duck.network

import io.github.banilaste.duck.DuckProperties
import io.github.banilaste.duck.skeleton.Skeleton
import io.github.banilaste.duck.util.LOGGER
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.nio.ByteBuffer
import kotlin.concurrent.thread

object DatagramHandler {
    private lateinit var socket: DatagramSocket
    private val messages = HashMap<String, Request>()
    private var callIndex = System.currentTimeMillis().toInt()
    private var stop = false

    // Content settings
    private const val BUFFER_SIZE = 25565
    // 1 for byte messageType, 4 for int callId, 4 for int attempt, 4 for int packetNumber
    private const val HEADER_SIZE = 13

    fun init() {
        // Create socket using the provided factory
        socket = DuckProperties.socketFactory(DuckProperties.localPort)

        thread(start = true) {
            stop = false

            LOGGER.info("starting server on port ${DuckProperties.localPort}")
            while (!stop) {
                val bytes = ByteArray(BUFFER_SIZE)
                val buffer = ByteBuffer.wrap(bytes)
                val packet = DatagramPacket(bytes, bytes.size)

                // Receive new packet and reset buffer
                socket.receive(packet)
                buffer.position(0)

                thread(start = true) {
                    handleDatagram(
                        buffer,
                        packet.address,
                        packet.port,
                        packet.length
                    )
                }
            }

            socket.close()
        }
    }

    fun stopServer() {
        stop = true
    }

    private fun handleDatagram(buffer: ByteBuffer, source: InetAddress, port: Int, dataSize: Int) {
        // Read content from buffer
        val callId = buffer.int
        val attempt = buffer.int
        val packetNumber = buffer.int
        val isLast = buffer.get() != 0.toByte()
        val id = "$callId$source$port-$attempt"

        val data = ByteArray(dataSize - HEADER_SIZE) // content - 9 octet for headers
        buffer.get(data)

        LOGGER.debug("[${source.hostName}:$port] received packet, callId = $callId, attempt = $attempt, packetNumber = $packetNumber, last = $isLast, data = {${data.joinToString()}}")

        // Retrieve of create message
        if (!messages.containsKey(id)) {
            messages[id] = Request()
        }
        val message = messages[id]!!

        // Prevent the answer to be given more than one time
        synchronized(message) {
            // Apply content to data
            message.put(packetNumber, data, isLast)

            // Trigger method
            if (message.done()) {
                message.triggered = System.currentTimeMillis()

                // Remove messages from cache
                messages.remove(id)

                // Compute request
                val dataResult = Skeleton.handle(callId, "$callId$source$port", message.compile())

                // Send result
                dataResult?.let {
                    send(
                        callId,
                        it,
                        source,
                        port,
                        attempt
                    )
                }
            }
        }
    }

    /**
     * Send new request, generate a request id and return it
     */
    fun send(content: ByteArray, destination: InetAddress, port: Int): Int {
        return send(
            callIndex++,
            content,
            destination,
            port,
            0
        )
    }

    /**
     * Sand answer to existing request
     */
    fun send(callId: Int, content: ByteArray, destination: InetAddress, port: Int, attempt: Int): Int {
        val sendingBuffer = ByteBuffer.allocate(BUFFER_SIZE)
        val packets = content.size / (BUFFER_SIZE - HEADER_SIZE) + 1

        repeat (packets) {
            val last = packets == it + 1
            val size = if (last) content.size - (BUFFER_SIZE - HEADER_SIZE) * it
                else BUFFER_SIZE - HEADER_SIZE
            val slicedContent = ByteBuffer.allocate(size)
                .put(content, (BUFFER_SIZE - HEADER_SIZE) * it, size)
                .array()

            sendingBuffer.position(0)
            sendingBuffer.putInt(callId)
                .putInt(attempt)
                .putInt(it)
                .put((if (last) 1 else 0).toByte())
                .put(slicedContent)


            LOGGER.debug("[${destination.hostName}:$port] sending packet, callId = $callId, attempt = $attempt, packetNumber = $it, last = $last, data = {${slicedContent.joinToString()}}")
            socket.send(DatagramPacket(sendingBuffer.array(), size + HEADER_SIZE, destination, port))
        }

        return callId
    }
}